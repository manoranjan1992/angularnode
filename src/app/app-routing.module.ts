import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventBookingComponent } from './components/event-booking/event-booking.component';
import { EventListingComponent } from './components/event-listing/event-listing.component';

const routes: Routes = [
  {
    path:"",pathMatch:"full",redirectTo:"event-booking"
  },{
    path:"event-booking",component:EventBookingComponent
  },{
    path:"event-list",component:EventListingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }