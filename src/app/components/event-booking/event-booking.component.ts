import { Router } from '@angular/router';
import { ApiService } from './../../service/api.service';
import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";

@Component({
  selector: 'app-event-booking',
  templateUrl: './event-booking.component.html',
  styleUrls: ['./event-booking.component.css']
})
export class EventBookingComponent implements OnInit {

  submitted = false;
  eventForm: FormGroup;
  eventNameList:any = [{"name":"Event1","seat":25},{"name":"Event2","seat":25},{"name":"Event3","seat":25},{"name":"Event4","seat":25},{"name":"Event5","seat":25}]
  selectedEvent:any = '';
  noOfSeatList:any = [1,2,3,4,5,6];
  model;
  randomEventName = "Event";
  randomAvailableSeat = 0;
  attendee:any = [];
  @ViewChild('f') form; 
  constructor(
    public fb: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private apiService: ApiService
  ) { 
    this.mainForm();
  }

  ngOnInit() { 

    var randomnumber = Math.floor(Math.random() * (5 - 1 + 1)) + 1;
    this.randomEventName =  randomnumber.toString();
    console.log(this.randomEventName);
    this.randomAvailableSeat =  Math.floor(Math.random() * (25 - 5 + 1)) + 1;
    console.log(this.randomAvailableSeat);
  }

  mainForm() {
    this.eventForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      noOfSeat: ['', [Validators.required]],
      date:['',[Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      attende:this.fb.array([this.attendeNameList()])
    })
  }
  attendeNameList(){
    return this.fb.group({
      name:['', [Validators.required]]
    })
  }
  get attendeForms() {
    return this.eventForm.get('attende') as FormArray;
  }

  // Choose event with select dropdown
  updateEvent(event:Event){
    // this.selectedEvent =  e;
    console.log(this.selectedEvent);
    // console.log(e)
    // this.eventForm.get('designation').setValue(e, {
    //   onlySelf: true
    // })
  }
  selectNoofseat(e){
    this.eventForm.get('noOfSeat').setValue(e, {
        onlySelf: true
      })
      this.attendee = [];
      for(var i=1;i<parseInt(e);i++){
        // this.attendee.push(i);
        this.addAttender();
      }
      // console.log(this.attendee);
  }

  addAttender() {
    const control = <FormArray>this.eventForm.controls['attende'];
    const att = this.fb.group({
      name:['', [Validators.required]]
    });

    control.push(att);
  }
  

  // Getter to access form control
  get myForm(){
    return this.eventForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.eventForm.valid) {
      return false;
    } else {
      var data = {
        "name":this.eventForm.get('name').value,
        "email": this.eventForm.get('email').value,
        "noOfSeat":this.eventForm.get('noOfSeat').value,
        "phoneNumber":this.eventForm.get('phoneNumber').value,
        "date": this.eventForm.get('date').value['year'] +"-"+ this.eventForm.get('date').value['month']+"-"  + this.eventForm.get('date').value['day'] 
      }
      this.apiService.createEvent(data).subscribe(
        (res) => {
          console.log('event successfully created!');
         
          this.form.resetForm();
          this.eventForm.reset();
          this.ngZone.run(() => this.router.navigateByUrl('/event-list'))
        }, (error) => {
          console.log(error);
        });
    }
  }

}
