import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-event-listing',
  templateUrl: './event-listing.component.html',
  styleUrls: ['./event-listing.component.css']
})
export class EventListingComponent implements OnInit {
Eventlist:any = [];
searchTerm:any;
EventlistCopy:any;
  constructor(private apiService: ApiService) { 
    this.getEventList();
  }

  ngOnInit(): void {
    console.log("event list");
  }

  getEventList(){
    this.apiService.getEvents().subscribe((data) => {
      this.Eventlist = data;
      this.EventlistCopy =  data;
     }) 
  }
  search(): void {
    let term = this.searchTerm;
    this.Eventlist = this.EventlistCopy.filter(function(tag) {
        return tag.name.indexOf(term) >= 0;
    }); 
}

}
