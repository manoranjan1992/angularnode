import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  @Input() event;
  constructor( private router: Router,
    private ngZone: NgZone,) { }

  ngOnInit(): void {
    console.log(this.event);
  }

  book(){
    this.ngZone.run(() => this.router.navigateByUrl('/event-booking'))
  }

}
