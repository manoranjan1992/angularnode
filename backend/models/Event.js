const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Event = new Schema({
   name: {
      type: String
   },
   email: {
      type: String
   },
   noOfSeat: {
      type: String
   },
   phoneNumber: {
      type: String
   },
   date: {
    type: String
 }
}, {
   collection: 'Events'
})


module.exports = mongoose.model('Event', Event)