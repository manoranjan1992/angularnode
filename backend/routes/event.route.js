const express = require('express');
const app = express();
const eventRoute = express.Router();

// Event model
let Event = require('../models/Event');

// Add Event
eventRoute.route('/add').post((req, res, next) => {
  Event.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get All Events
eventRoute.route('/').get((req, res) => {
  Event.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})






module.exports = eventRoute;